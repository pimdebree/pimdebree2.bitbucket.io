
const height = 50;
const width = 50;
var direction_x = 0;
var direction_y = 0;
var gamespeed = 50;

var snake_coordinates = new Array();

function createMap() {

   var table = document.createElement("TABLE");

   for (var y = 0; y < height; y++) {
       var tr = document.createElement("TR");
       table.appendChild(tr);
       for (var x = 0; x < width; x++) {
            var td = document.createElement("TD");
            td.setAttribute("id", x+"_"+y);

            var text = document.createTextNode(x+"_"+y);

            td.appendChild(text);
            if (x == 0 || y == 0 || x < width && y == (height-1) || y < height && x == (width-1)) {
                // document.write("<td class='wall'>"+x+" "+y+"</td>");
                td.classList.add("wall");
            } else {
                td.classList.add("block");
            }
            tr.appendChild(td);
       }
       document.write("</tr>");
   }
   document.body.appendChild(table);
   document.write("</table>");
}

function mainLoop() {
    moveSnake();
}

function moveSnake() {

    
    
}

//zodat je kan luisteren of een key wordt gedrukt of niet
document.addEventListener('keydown', function(event) {
    // console.log(event.keyCode);

    switch (event.keyCode) {
        case 38:
        //moet direction x naar 0 zetten, gaat anders horizontaal
            direction_x = 0;
            direction_y = 1;
        break;

        case 40:
        //moet direction x naar 0 zetten, gaat anders horizontaal
            direction_x = 0;
            direction_y = -1;
        break;

        case 37:
        //moet direction y naar 0 zetten, gaat anders horizontaal
            direction_y = 0;
            direction_x = 1;
        break;

        case 39:
        //moet direction x naar 0 zetten, gaat anders horizontaal
            direction_y = 0;
            direction_x = -1;
        break;
    }
}, false);

function randomValue(value) {
    return Math.floor(Math.random() * value);
}

function start() {
    createMap();

    // snake_coordinates.push(randomValue(width), randomValue(height));
    snake_coordinates.push([4, 4], [4, 2]);

    setInterval(mainLoop, gamespeed);
}

start();


