<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>PRC 1.1 | Adra Philipse</title>

    <!-- css -->
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" type="text/css" href="css/gamestyle.css" />
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
	<!-- css -->
    <!-- compatibility -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <!-- compatibility -->

    <!-- js -->
    <script src="js/app.js" defer></script>
  </head>
    <body>
    <section class="gamefield">
        <h1>Who's that Pokemon?</h1>
        
        <section class="who">
          <div id="whois"></div>
        </section>

        <section class="grid">
          <div id="gridplace">

          </div> 
        </section>     
    </section>
    </body>
</html>