for (var z = 0; z < snake_coordinates.length; z++) {
        //krijgt hierdoor de variable van het element van de coordinaten van de snake
        var snake = document.getElementById(snake_coordinates[z][0]+"_"+snake_coordinates[z][1]);

        console.log(snake_coordinates[z][0]+" "+snake_coordinates[z][1]);
        
        //stopt deze variablen in een 2 tal variabelen voor later gebruik
        var old_x = snake_coordinates[z][0];
        var old_y = snake_coordinates[z][1];

        //verwijdert de laatste 2 stukjes van de snake
        // snake_coordinates.splice(snake_coordinates.length - 1, old_x);
        // snake_coordinates.splice(snake_coordinates.length - 1, old_y);
        snake_coordinates.shift();

        //voegt nu weer 2 stukjes toe
        // snake_coordinates.push(old_x, old_y);

        var dir_x = old_x - direction_x;
        var dir_y = old_y - direction_y;

        //en voegt ze weer toe
        // snake_coordinates.push(dir_x, dir_y);
        snake_coordinates.push([dir_x, dir_y]);

        // console.log("dirx: "+direction_x+" diry: "+direction_y+" posx: "+dir_x+" posy: "+dir_y);
        
        if (dir_x == 1 && direction_x == 1) {
            direction_x = -1;
            direction_y = 0;
        } else if (dir_y == 1 && direction_y == 1) {
            direction_x = 0;
            direction_y = -1;
        } else if (dir_y == (height - 2) && direction_y == -1) {
            direction_x = 0;
            direction_y = 1;
        } else if (dir_x == (width - 2) && direction_x == -1) {
            direction_x = 1;
            direction_y = 0;
        }

        //logt de coordinaten van de snake
        // console.log(snake_coordinates.length);

        //block toevoegen -> snake verwijderen
        snake.classList.add("block");
        snake.classList.remove("snake");
        
        //pakt de nieuwe coordinaten van de snake op de coordinaten in de array
        snake = document.getElementById(snake_coordinates[z][0]+"_"+snake_coordinates[z][1]);
        
        //block verwijderen -> snake toevoege
        snake.classList.remove("block");
        snake.classList.add("snake");
     }